from decouple import config
from flask import Flask, request, jsonify, make_response
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_jwt_extended import create_access_token, create_refresh_token
from flask_jwt_extended import JWTManager
from flask_jwt_extended import jwt_required


app = Flask(__name__)
jwt = JWTManager(app)

POSTGRES = {
    "user": config('user'),
    "pw": config("pw"),
    "db": config("db"),
    "host": config("host"),
    "port": "5432",
}


app.config["SQLALCHEMY_DATABASE_URI"] = (
    "postgresql://%(user)s:\
%(pw)s@%(host)s:%(port)s/%(db)s"
    % POSTGRES
)

app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SECRET_KEY"] = "Sm9obiBTY2hyb20ga2lja3MgYXNz"

db = SQLAlchemy(app)
ma = Marshmallow(app)


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100))
    description = db.Column(db.String(200))
    author = db.Column(db.String(50))

    def __init__(self, title, description, author):
        self.title = title
        self.description = description
        self.author = author


class PostSchema(ma.Schema):
    class Meta:
        fields = ("title", "author", "description")


post_schema = PostSchema()
posts_schema = PostSchema(many=True)


@app.route("/put", methods=["POST"])
@jwt_required()
def add_post():
    title = request.json["title"]
    description = request.json["description"]
    author = request.json["author"]

    my_posts = Post(title, description, author)
    db.session.add(my_posts)
    db.session.commit()

    return post_schema.jsonify(my_posts)


@app.route("/get", methods=["GET"])
@jwt_required()
def get_post():
    all_posts = Post.query.all()
    result = posts_schema.dump(all_posts)

    return jsonify(result)


@app.route("/post_details/<id>/", methods=["GET"])
def post_details(id):
    post = Post.query.get(id)
    return post_schema.jsonify(post)


@app.route("/update/<id>/", methods=["PUT"])
@jwt_required()
def post_update(id):
    post = Post.query.get(id)

    title = request.json["title"]
    description = request.json["description"]
    author = request.json["author"]

    post.title = title
    post.description = description
    post.author = author

    db.session.commit()
    return post_schema.jsonify(post)


@app.route("/delete/<id>/", methods=["DELETE"])
@jwt_required()
def post_delete(id):
    post = Post.query.get(id)
    db.session.delete(post)
    db.session.commit()

    return post_schema.jsonify(post)


@app.route("/login", methods=["GET"])
def login():
    auth = request.authorization
    if auth and auth.password == "password":
        user = auth
        access_token = create_access_token(identity=user.username, fresh=True)
        refresh_token = create_refresh_token(user.username)
        return {"access_token": access_token,
                "refresh_token": refresh_token}, 200

    else:
        return make_response("Unable to verify"), 403


if __name__ == "__main__":
    app.run(debug=True)
